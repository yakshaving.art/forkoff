package main

import (
	"fmt"
	"log"
	"os"

	"github.com/cenkalti/backoff"
	gitlab "github.com/xanzy/go-gitlab"
)

var client *gitlab.Client

func main() {
	args := ParseArgs()

	client = gitlab.NewClient(nil, args.GitlabToken())
	client.SetBaseURL(args.GitlabURL())

	var maintainers []maintainerID

	for _, maintainer := range args.Maintainers() {
		log.Printf("Loading user %s", maintainer)

		u, _, err := client.Users.ListUsers(&gitlab.ListUsersOptions{
			Username: &maintainer,
		})
		if err != nil {
			log.Fatalf("failed while validating user %s: %s", maintainer, err)
		}
		if len(u) == 0 {
			log.Fatalf("could not find user %s", maintainer)
		}
		if len(u) != 1 {
			log.Fatalf("found more than 1 user with %s: %v", maintainer, u)
		}
		maintainers = append(maintainers, maintainerID{
			ID:   &u[0].ID,
			Name: maintainer})
	}

	log.Printf("Loading source repo %s", args.SourceRepo)
	_, err := fetchProject(args.SourceRepo)
	if err != nil {
		log.Fatalf("Invalid source repo %s: %s", args.SourceRepo, err)
	}
	if targetRepo, _ := fetchProject(fmt.Sprintf("%s/%s", args.TargetNamespace, args.TargetRepoName)); targetRepo != nil {
		log.Fatalf("Target repo %s/%s already exists", args.TargetNamespace, args.TargetRepoName)
		os.Exit(1)
	}

	if args.DryRun {
		log.Println("Everything looks fine, quitting due to dryrun execution")
		os.Exit(0)
	}

	log.Printf("Forking repo %s into %s/%s", args.SourceRepo, args.TargetNamespace, args.TargetRepoName)
	var path string
	if args.TargetRepoName != "" {
		path = args.TargetRepoName
	}

	fargs := forkingArgs{
		retry:           3,
		source:          args.SourceRepo,
		targetNamespace: &args.TargetNamespace,
		targetPath:      &path,
	}
	p, err := forkWithRetries(fargs)
	if err != nil {
		log.Fatalf("Failed to fork repo %s into %s: %s", args.SourceRepo,
			fargs.target(), err)
	}

	if args.Detach {
		log.Printf("Removing forking relationship in %s", p.PathWithNamespace)
		if err := detachProject(p.ID); err != nil {
			log.Fatalf("failed to remove fork relationship of project %s: %s", p.NameWithNamespace, err)
		}
	}

	if args.Description != "" {
		log.Printf("Setting project description to %s", args.Description)
		newP, _, err := client.Projects.EditProject(p.ID, &gitlab.EditProjectOptions{
			Description: &args.Description,
		})
		if err != nil {
			log.Fatalf("failed to set description of project %s to %q: %s", p.NameWithNamespace, p.Description, err)
		}

		p = newP
	}

	maintainerLevel := gitlab.MaintainerPermissions
	for _, maintainer := range maintainers {
		log.Printf("Adding maintainer %s to %s", maintainer.Name, p.NameWithNamespace)
		if _, _, err := client.ProjectMembers.AddProjectMember(p.ID, &gitlab.AddProjectMemberOptions{
			UserID:      maintainer.ID,
			AccessLevel: &maintainerLevel,
		}); err != nil {
			log.Fatalf("failed to set user as maintainer of %s", p.NameWithNamespace)
		}
	}
}

type maintainerID struct {
	ID   *int
	Name string
}

type forkingArgs struct {
	retry int

	source          string
	targetNamespace *string
	targetPath      *string
}

func (f forkingArgs) target() string {
	return fmt.Sprintf("%s/%s", *f.targetNamespace, *f.targetPath)
}

// forkWithRetries
// Concepts I need to handle are:
//   forking: can fail but does it fast
//   checking the tree: can fail or return a 404 for a while, or forever
// in failure of checking the tree, I need to delete and start again
func forkWithRetries(args forkingArgs) (*gitlab.Project, error) {
	for retry := 0; retry < 5; retry++ {
		p, err := forkProject(args)
		if err != nil {
			return nil, err
		}

		err = backoff.Retry(func() error {
			log.Printf("checking forked repo %s is correct", args.target())
			err := checkLSTreeFromRepo(p.ID)
			if err == nil {
				log.Printf("forked repo %s contains files, thus is was correctly forked", args.target())
				return nil
			}
			log.Printf("forked repo %s failed the check: %s", args.target(), err)
			return err
		}, backoff.WithMaxRetries(backoff.NewExponentialBackOff(), 10))

		if err == errEmptyForkedRepo {
			log.Printf("forked repo %s is empty, deleting and forking %s again", args.target(), args.source)
			if err := deleteProject(p.ID); err != nil {
				return nil, fmt.Errorf("could not delete broken repo for _reasons_: %s", err)
			}
			continue
		}

		return p, nil
	}
	return nil, errEmptyForkedRepo
}

var errEmptyForkedRepo = fmt.Errorf("repository is empty")

func checkLSTreeFromRepo(pid int) error {
	// If forking fails, listing the tree of the repo will return a 404
	_, resp, err := client.Repositories.ListTree(pid, &gitlab.ListTreeOptions{})
	if err != nil {
		return err
	}
	switch resp.StatusCode {
	case 404:
		return errEmptyForkedRepo
	case 200:
		return nil
	default:
		return fmt.Errorf("unexpected status code %s", resp.Status)
	}
}

func forkProject(args forkingArgs) (*gitlab.Project, error) {
	p, _, err := client.Projects.ForkProject(args.source, &gitlab.ForkProjectOptions{
		Namespace: args.targetNamespace,
		Name:      args.targetPath,
		Path:      args.targetPath,
	})
	if err != nil {
		return nil, err
	}
	return p, nil
}

func deleteProject(pid int) error {
	_, err := fetchProject(pid)
	if err != nil {
		return fmt.Errorf("could not delete project, failed to fetch it first: %s", err)
	}

	_, err = client.Projects.DeleteProject(pid)
	return err
}

var errProjectNotFound = fmt.Errorf("could not find project")

func fetchProject(pid interface{}) (*gitlab.Project, error) {
	repo, res, err := client.Projects.GetProject(pid, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to find project %s: %s", pid, err)
	}
	if res.StatusCode == 404 {
		return nil, errProjectNotFound
	}

	return repo, nil
}

func detachProject(pid int) error {
	if _, err := client.Projects.DeleteProjectForkRelation(pid); err != nil {
		return fmt.Errorf("failed to remove fork relationship: %s", err)
	}
	return nil
}
