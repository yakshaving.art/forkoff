FROM alpine:3.8

RUN apk --no-cache add ca-certificates libc6-compat

COPY forkoff /
 
ENTRYPOINT [ "/forkoff" ]
